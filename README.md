# README #

## Weather App with forecast feature.

1. Weather of current location
1. Weather of current day
1. Forecast of 16 days (can be changed from 5 to 16 days)
1. Show weather detail of five days in landscape screen (every three hours)
1. Celsius / Fahrenheit switch
1. Night/Day icons for night/day time



* Use MVVM architecture
* Single way dependency (Downward: View -> ViewModel -> Model)
* Upward communication by events(Model -- modelEventBus >> ViewModel -- viewModelEvent >> View)
* Query classes encapsulte the operations of Model object
* Late data bind
* Use "http://api.openweathermap.org/" as backend
* Dagger2 for Dependency Inject
* ** Use local mock API when productFlavor is "develop" **




### How do I get set up? ###

* git clone git@bitbucket.org:zhenguo_fu/weather.git
* open the directory in Android Studio








### What is this repository for? ###

* Quick summary
* Version


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact