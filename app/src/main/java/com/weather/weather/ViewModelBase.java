package com.weather.weather;


import android.app.Activity;

import com.weather.weather.ViewModel.IModelEventBusListener;

import org.greenrobot.eventbus.EventBus;

public abstract class ViewModelBase {
    protected Activity activity;
    protected EventBus viewModelEventBus;
    protected EventBus modelEventBus;
    protected IModelEventBusListener modelEventBusListener;

    public ViewModelBase(Activity activity, EventBus viewModelEventBus) {
        this.viewModelEventBus = viewModelEventBus;
        this.activity = activity;

        modelEventBus = EventBus.builder().build();
        modelEventBusListener = createModelEventBusListener();
        modelEventBus.register(modelEventBusListener);
    }

    protected abstract void configStateMachine();
    protected abstract IModelEventBusListener createModelEventBusListener();

    public void onDestroy() {
        modelEventBus.unregister(modelEventBusListener);
    }

}
