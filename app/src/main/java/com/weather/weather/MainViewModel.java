package com.weather.weather;

import android.app.Activity;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;
import com.github.oxo42.stateless4j.delegates.Action1;
import com.github.oxo42.stateless4j.triggers.TriggerWithParameters1;
import com.orhanobut.logger.Logger;
import com.weather.weather.Model.Location;
import com.weather.weather.Model.LocationList;
import com.weather.weather.Model.LocationQuery;
import com.weather.weather.Model.WeatherDetail;
import com.weather.weather.Model.WeatherDetailList;
import com.weather.weather.Model.WeatherQuery;
import com.weather.weather.ViewModel.IModelEventBusListener;
import com.weather.weather.ViewModel.WeatherDetailInfo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainViewModel extends ViewModelBase {
    private static final long LOCATION_REFRESH_INTERVAL = 30000;

    private StateMachine<State, Trigger> stateMachine;
    private TriggerWithParameters1<Integer, State, Trigger> TRIGGER_FOR_USER_REQUEST_DAY_FORECAST;

    private LocationList locationList;
    private LocationQuery locationQuery;
    private WeatherDetailList currentWeatherDetailList;
    private WeatherDetailList forecastWeatherDetailList;
    private WeatherDetailList forecastWeatherDailyList;
    private WeatherQuery weatherQuery;
    private Map<Integer, List<WeatherDetail>> weatherForecastMap;

    private Location currentLocation;
    private WeatherDetail currentWeatherDetail;
    private WeatherDetailInfo.TEMPERATURE_TYPE temperatureType = WeatherDetailInfo.TEMPERATURE_TYPE.CELSIUS;

    public MainViewModel(Activity activity, EventBus viewModelEventBus) {
        super(activity, viewModelEventBus);
        Logger.d("activity: %s, viewModelEventBus: %s", activity, viewModelEventBus);

        configStateMachine();

        locationList = new LocationList();
        locationQuery = new LocationQuery(activity,
                modelEventBus,
                locationList,
                LOCATION_REFRESH_INTERVAL);
        weatherQuery = new WeatherQuery(activity, modelEventBus);
        weatherForecastMap = new HashMap<>();
    }

    @Override
    public void onDestroy() {
        Logger.d("");
        super.onDestroy();
    }

    @Override
    protected void configStateMachine() {
        Logger.d("");
        stateMachine = new StateMachine<>(State.WAITING_FOR_LOCATION);
        TRIGGER_FOR_USER_REQUEST_DAY_FORECAST =
                stateMachine.setTriggerParameters(Trigger.FORECAST_DETAIL_REQUESTED, Integer.class);
        Action actionForFetchWeatherDetail = new Action() {
            @Override
            public void doIt() {
                Logger.d("");
                if (!locationList.getList().isEmpty()) {
                    currentLocation = locationList.getList().get(0);
                    currentWeatherDetailList = new WeatherDetailList();
                    weatherQuery.queryCurrent(currentLocation, currentWeatherDetailList);
                } else {
                    viewModelEventBus.post(new LocationNotAvailable());
                }
            }
        };

        Action actionForUpdateCurrentWeather = new Action() {
            @Override
            public void doIt() {
                Logger.d("");
                currentWeatherDetail = currentWeatherDetailList.getList().get(0);
                WeatherDetailInfo weatherDetailInfo = WeatherDetailInfo.convertWeatherDetailInfo(
                        currentWeatherDetail,
                        temperatureType);
                viewModelEventBus.post(weatherDetailInfo);

                forecastWeatherDetailList = new WeatherDetailList();
                weatherQuery.queryForecastDetail(currentLocation, forecastWeatherDetailList);
            }
        };

        Action actionForUpdateForecastDetail = new Action() {
            @Override
            public void doIt() {
                Logger.d("");
                processForecastDetail(forecastWeatherDetailList, weatherForecastMap);
                postWeatherInfoListOfDay(currentWeatherDetail.getDateTime().toLocalDate().getDayOfYear());

                forecastWeatherDailyList = new WeatherDetailList();
                weatherQuery.queryForecastDaily(currentLocation, forecastWeatherDailyList);
            }
        };

        Action actionForUpdateForecastDaily = new Action() {
            @Override
            public void doIt() {
                Logger.d("");
                ForecastDailyInfo forecastDailyInfo = new ForecastDailyInfo();
                convertForecastDaily(forecastWeatherDailyList.getList(), forecastDailyInfo);
                viewModelEventBus.post(forecastDailyInfo);

                WeatherDetailInfo currentWeatherDetailInfo = WeatherDetailInfo.convertWeatherDetailInfo(
                        currentWeatherDetail,
                        temperatureType);
                updateCurrentWeatherDetailInfo(currentWeatherDetailInfo, forecastDailyInfo.weatherDetailInfoList);
                viewModelEventBus.post(currentWeatherDetailInfo);

                viewModelEventBus.post(new TemperatureTypeReady());
            }
        };


        Action1<Integer> actionForUserOperation = new Action1<Integer>() {
            @Override
            public void doIt(Integer dayOfYear) {
                Logger.d("dayOfYear: %s", dayOfYear);
                postWeatherInfoListOfDay(dayOfYear);
                for (WeatherDetail weatherDetail : forecastWeatherDailyList.getList()) {
                    if (dayOfYear == weatherDetail.getDateTime().getDayOfYear()) {
                        currentWeatherDetail = weatherDetail;
                        WeatherDetailInfo weatherDetailInfo = WeatherDetailInfo.convertWeatherDetailInfo(
                                currentWeatherDetail,
                                temperatureType);
                        viewModelEventBus.post(weatherDetailInfo);
                        break;
                    }
                }
            }
        };

        Action actionForLocationReceived = new Action() {
            @Override
            public void doIt() {
                Logger.d("currentLocation: %s", currentLocation);
                if (!locationList.getList().get(0).equals(currentLocation)) {
                    stateMachine.fire(Trigger.LOCATION_CHANGED_REQUESTED);
                }
            }
        };

        Action actionForChangeTemperatureType = new Action() {
            @Override
            public void doIt() {
                postWeatherInfoDailyList();
                postWeatherInfoListOfDay(currentWeatherDetail.getDateTime().toLocalDate().getDayOfYear());
            }
        };

        stateMachine.configure(State.WAITING_FOR_LOCATION)
                .ignore(Trigger.FORECAST_DETAIL_REQUESTED)
                .ignore(Trigger.CURRENT_WEATHER_DETAIL_RECEIVED)
                .ignore(Trigger.TEMPERATURE_TYPE_CHANGE_REQUESTED)
                .onExit(actionForFetchWeatherDetail)
                .permit(Trigger.LOCATION_RECEIVED, State.WAITING_FOR_WEATHER_DETAIL);
        stateMachine.configure(State.WAITING_FOR_WEATHER_DETAIL)
                .onEntryFrom(Trigger.LOCATION_RECEIVED, actionForLocationReceived)
                .onEntryFrom(Trigger.LOCATION_CHANGED_REQUESTED, actionForFetchWeatherDetail)
                .onEntryFrom(Trigger.CURRENT_WEATHER_DETAIL_RECEIVED, actionForUpdateCurrentWeather)
                .onEntryFrom(Trigger.FORECAST_WEATHER_DETAIL_RECEIVED, actionForUpdateForecastDetail)
                .onEntryFrom(Trigger.FORECAST_WEATHER_DAILY_RECEIVED, actionForUpdateForecastDaily)
                .onEntryFrom(Trigger.TEMPERATURE_TYPE_CHANGE_REQUESTED, actionForChangeTemperatureType)
                .onEntryFrom(TRIGGER_FOR_USER_REQUEST_DAY_FORECAST, actionForUserOperation, Integer.class)
                .permitReentry(Trigger.LOCATION_CHANGED_REQUESTED)
                .permitReentry(Trigger.LOCATION_RECEIVED)
                .permitReentry(Trigger.CURRENT_WEATHER_DETAIL_RECEIVED)
                .permitReentry(Trigger.FORECAST_WEATHER_DETAIL_RECEIVED)
                .permitReentry(Trigger.FORECAST_WEATHER_DAILY_RECEIVED)
                .permitReentry(Trigger.TEMPERATURE_TYPE_CHANGE_REQUESTED)
                .permitReentry(Trigger.FORECAST_DETAIL_REQUESTED);
    }

    private void postWeatherInfoDailyList() {
        ForecastDailyInfo forecastDailyInfo = new ForecastDailyInfo();
        convertForecastDaily(forecastWeatherDailyList.getList(), forecastDailyInfo);
        viewModelEventBus.post(forecastDailyInfo);

        // Cause the current weather API of backend just can give max and min of current time rather than current day
        // We have to update the temperature of current date by the forecast
        WeatherDetailInfo currentWeatherDetailInfo = WeatherDetailInfo.convertWeatherDetailInfo(
                currentWeatherDetail,
                temperatureType);
        updateCurrentWeatherDetailInfo(currentWeatherDetailInfo, forecastDailyInfo.weatherDetailInfoList);
        viewModelEventBus.post(currentWeatherDetailInfo);
    }

    private void updateCurrentWeatherDetailInfo(WeatherDetailInfo currentWeatherDetailInfo, List<WeatherDetailInfo> list) {
        for (WeatherDetailInfo weatherDetailInfo : list) {
            if (currentWeatherDetailInfo.getDayOfYear() == weatherDetailInfo.getDayOfYear()) {
                currentWeatherDetailInfo.setMaxT(weatherDetailInfo.getMaxT());
                currentWeatherDetailInfo.setMinT(weatherDetailInfo.getMinT());
            }
        }
    }

    private void postWeatherInfoListOfDay(int dayOfYear) {
        List<WeatherDetail> weatherDetailList = weatherForecastMap.get(dayOfYear);
        ForecastDetailInfo forecastDetailInfo = new ForecastDetailInfo();
        if (weatherDetailList == null) {
            Logger.d("No data for this day: %s", dayOfYear);
            weatherDetailList = new ArrayList<>();
        }

        convertForecastDaily(weatherDetailList, forecastDetailInfo);
        viewModelEventBus.post(forecastDetailInfo);
    }

    private void processForecastDetail(WeatherDetailList forecastWeatherList,
                                       Map<Integer, List<WeatherDetail>> weatherDetailMap) {
        Logger.d("list: %s, array: %s", forecastWeatherList, weatherDetailMap);
        for (WeatherDetail weatherDetail : forecastWeatherList.getList()) {
            int dayOfDetail = weatherDetail.getDateTime().toLocalDate().getDayOfYear();
            List<WeatherDetail> weatherDetailList = weatherDetailMap.get(dayOfDetail);
            if (weatherDetailList == null) {
                weatherDetailList = new ArrayList<>();
                weatherDetailMap.put(dayOfDetail, weatherDetailList);
            }

            weatherDetailList.add(weatherDetail);
        }
    }

    public void requestForecast(int dayOfYear) {
        stateMachine.fire(TRIGGER_FOR_USER_REQUEST_DAY_FORECAST, dayOfYear);
    }

    public void requestChangeTemperatureType(WeatherDetailInfo.TEMPERATURE_TYPE temperatureType) {
        this.temperatureType = temperatureType;
        stateMachine.fire(Trigger.TEMPERATURE_TYPE_CHANGE_REQUESTED);
    }

    private void convertForecastDaily(List<WeatherDetail> weatherDetailList, ForecastBase forecastBase) {
        Logger.d("list: %s, forecast: %s", weatherDetailList, forecastBase);
        List<WeatherDetailInfo> weatherDetailInfoList = forecastBase.getWeatherDetailInfoList();
        for (WeatherDetail weatherDetail : weatherDetailList) {
            WeatherDetailInfo weatherDetailInfo = WeatherDetailInfo.convertWeatherDetailInfo(weatherDetail, temperatureType);
            weatherDetailInfoList.add(weatherDetailInfo);
        }
    }

    @Override
    protected IModelEventBusListener createModelEventBusListener() {
        return new IModelEventBusListener<LocationList>() {
            @Override
            @Subscribe(threadMode = ThreadMode.MAIN) // Check if location changed may take time
            public void onMessage(LocationList theLocationList) {
                Logger.d(theLocationList);
                if (theLocationList == locationList) {
                    stateMachine.fire(Trigger.LOCATION_RECEIVED);
                }
            }

            @Subscribe(threadMode = ThreadMode.MAIN)
            public void onMessage(WeatherDetailList theWeatherDetailList) {
                Logger.d(theWeatherDetailList);
                if (theWeatherDetailList == currentWeatherDetailList) {
                    stateMachine.fire(Trigger.CURRENT_WEATHER_DETAIL_RECEIVED);
                } else if (theWeatherDetailList == forecastWeatherDetailList) {
                    stateMachine.fire(Trigger.FORECAST_WEATHER_DETAIL_RECEIVED);
                } else if (theWeatherDetailList == forecastWeatherDailyList) {
                    stateMachine.fire(Trigger.FORECAST_WEATHER_DAILY_RECEIVED);
                }
            }
        };
    }

    private enum State {
        WAITING_FOR_LOCATION,
        WAITING_FOR_WEATHER_DETAIL,
    }

    private enum Trigger {
        LOCATION_RECEIVED,
        CURRENT_WEATHER_DETAIL_RECEIVED,
        FORECAST_DETAIL_REQUESTED,
        LOCATION_CHANGED_REQUESTED,
        FORECAST_WEATHER_DETAIL_RECEIVED,
        FORECAST_WEATHER_DAILY_RECEIVED,
        TEMPERATURE_TYPE_CHANGE_REQUESTED,
    }

    public static class ForecastDetailInfo extends ForecastBase {
    }

    public static class ForecastDailyInfo extends ForecastBase {
    }

    private abstract static class ForecastBase {
        protected List<WeatherDetailInfo> weatherDetailInfoList;

        public ForecastBase() {
            this.weatherDetailInfoList = new ArrayList<>();
        }

        public List<WeatherDetailInfo> getWeatherDetailInfoList() {
            return weatherDetailInfoList;
        }
    }

    public static class LocationNotAvailable {
    }


    public static class TemperatureTypeReady {
    }

    public static class WeatherDetailAvailableInfo {
        private Set<Integer> availableDaySet;

        public WeatherDetailAvailableInfo(Set<Integer> detailAvailabeSet) {
            availableDaySet = detailAvailabeSet;
        }

        public Set<Integer> getAvailableDaySet() {
            return availableDaySet;
        }
    }
}
