package com.weather.weather.View;

import org.greenrobot.eventbus.EventBus;

public interface IEventBusHolder {
    EventBus getEventBus();
}
