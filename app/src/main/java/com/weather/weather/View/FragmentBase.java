package com.weather.weather.View;

import android.app.Fragment;
import android.content.Context;

import com.weather.weather.ViewModel.IViewModelEventBusListener;

import org.greenrobot.eventbus.EventBus;

public abstract class FragmentBase<T> extends Fragment implements IViewModelEventBusListener<T> {
    private EventBus eventBus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        IEventBusHolder eventBusHolder = (IEventBusHolder) getActivity();
        eventBus = eventBusHolder.getEventBus();
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }
}
