package com.weather.weather.View;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.weather.weather.ViewModel.IViewModelEventBusListener;
import com.weather.weather.ViewModelBase;

import org.greenrobot.eventbus.EventBus;

public abstract class ActivityBase<T extends ViewModelBase, EVENT> extends AppCompatActivity implements IViewModelEventBusListener<EVENT>{
    protected T viewModelBase;
    protected EventBus viewModelEventBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModelEventBus = EventBus.builder().logNoSubscriberMessages(false)
                .sendNoSubscriberEvent(false).build();
        viewModelEventBus.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        viewModelEventBus.unregister(this);
        viewModelBase.onDestroy();
    }
}
