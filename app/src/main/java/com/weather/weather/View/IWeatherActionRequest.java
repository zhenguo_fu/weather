package com.weather.weather.View;

import com.weather.weather.ViewModel.WeatherDetailInfo;

public interface IWeatherActionRequest {
    void getWeatherDayDetail(int dayOfYear);
    void requestChangeTemperatureType(WeatherDetailInfo.TEMPERATURE_TYPE temperatureType);
}
