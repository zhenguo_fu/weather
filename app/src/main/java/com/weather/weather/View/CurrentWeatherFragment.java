package com.weather.weather.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.weather.weather.MainViewModel;
import com.weather.weather.R;
import com.weather.weather.ViewModel.WeatherDetailInfo;
import com.weather.weather.WeatherApp;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class CurrentWeatherFragment extends FragmentBase<WeatherDetailInfo>{
    @InjectView(R.id.TV_LOCATION)
    TextView TV_LOCATION;
    @InjectView(R.id.TV_WEEKDAY)
    TextView TV_WEEKDAY;
    @InjectView(R.id.TV_MAX_TEMP)
    TextView TV_MAX_TEMP;
    @InjectView(R.id.TV_MIN_TEMP)
    TextView TV_MIN_TEMP;
    @InjectView(R.id.TV_CURRENT_TEMP)
    TextView TV_CURRENT_TEMP;
    @InjectView(R.id.TV_WEATHER)
    TextView TV_WEATHER;
    @InjectView(R.id.IV_WEATHER)
    ImageView IV_WEATHER;
    @InjectView(R.id.SW_TEMP_TYPE)
    Switch SW_TEMP_TYPE;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_weather, container);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SW_TEMP_TYPE.setEnabled(false);
        SW_TEMP_TYPE.setActivated(false);
        SW_TEMP_TYPE.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((IWeatherActionRequest) getActivity()).requestChangeTemperatureType((isChecked
                        ? WeatherDetailInfo.TEMPERATURE_TYPE.CELSIUS : WeatherDetailInfo.TEMPERATURE_TYPE.FAHRENHEIT));
            }
        });
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(WeatherDetailInfo weatherDetailInfo) {
        Logger.d(weatherDetailInfo);
        TV_LOCATION.setText(weatherDetailInfo.getLocation());
        TV_WEEKDAY.setText(weatherDetailInfo.getWeekAndDate());
        TV_MAX_TEMP.setText(weatherDetailInfo.getMaxT());
        TV_MIN_TEMP.setText(weatherDetailInfo.getMinT());
        TV_CURRENT_TEMP.setText(weatherDetailInfo.getCurrentT());
        TV_WEATHER.setText(weatherDetailInfo.getStatus());
        IV_WEATHER.setImageResource(WeatherApp.getStatusResId(weatherDetailInfo.getStatusType(),
                weatherDetailInfo.isNight()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MainViewModel.TemperatureTypeReady temperatureTypeReady) {
        Logger.d("");
        SW_TEMP_TYPE.setEnabled(true);
        SW_TEMP_TYPE.setActivated(true);
    }
}
