package com.weather.weather.View;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.weather.weather.MainViewModel;
import com.weather.weather.R;
import com.weather.weather.ViewModel.IViewModelEventBusListener;
import com.weather.weather.ViewModel.WeatherDetailInfo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends ActivityBase<MainViewModel, MainViewModel.LocationNotAvailable>
        implements IEventBusHolder, IViewModelEventBusListener<MainViewModel.LocationNotAvailable>, IWeatherActionRequest {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModelBase = new MainViewModel(this, viewModelEventBus);
        configUI(getResources().getConfiguration());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Logger.d("configuration: %s", newConfig);
        configUI(newConfig);
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MainViewModel.LocationNotAvailable locationNotAvailable) {
        Toast.makeText(this, getString(R.string.check_gps), Toast.LENGTH_SHORT).show();
    }

    @Override
    public EventBus getEventBus() {
        return viewModelEventBus;
    }

    @Override
    public void getWeatherDayDetail(int dayOfYear) {
        viewModelBase.requestForecast(dayOfYear);
    }

    @Override
    public void requestChangeTemperatureType(WeatherDetailInfo.TEMPERATURE_TYPE temperatureType) {
        viewModelBase.requestChangeTemperatureType(temperatureType);
    }

    private void configUI(Configuration configuration) {
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideFragment(R.id.FRAGMENT_CURRENT);
            showFragment(R.id.FRAGMENT_WEATHER_DETAIL);
        } else {
            hideFragment(R.id.FRAGMENT_WEATHER_DETAIL);
            showFragment(R.id.FRAGMENT_CURRENT);
        }
    }

    private void hideFragment(int fragmentId) {
        Fragment fragment = getFragmentManager().findFragmentById(fragmentId);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .hide(fragment)
                .commit();
    }

    private void showFragment(int fragmentId) {
        Fragment fragment = getFragmentManager().findFragmentById(fragmentId);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .show(fragment)
                .commit();
    }
}
