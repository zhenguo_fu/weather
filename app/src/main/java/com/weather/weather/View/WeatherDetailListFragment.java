package com.weather.weather.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.weather.weather.MainViewModel;
import com.weather.weather.R;
import com.weather.weather.ViewModel.WeatherDetailInfo;
import com.weather.weather.WeatherApp;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;


public class WeatherDetailListFragment extends FragmentBase<MainViewModel.ForecastDetailInfo> {
    private ExpandableHeightListView expandableHeightListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.d("%s, %s, %s", inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_weather_day_detail, container, false);
        expandableHeightListView = (ExpandableHeightListView) view.findViewById(R.id.LV_WEATHER_DAY_DETAIL);
        return view;
    }

    @Override
    @Subscribe(threadMode=ThreadMode.MAIN)
    public void onMessage(MainViewModel.ForecastDetailInfo forecastDetailInfo) {
        Logger.d(forecastDetailInfo);
        WeatherDetailAdapter weatherDetailAdapter = (WeatherDetailAdapter) expandableHeightListView.getAdapter();
        if (weatherDetailAdapter == null) {
            weatherDetailAdapter = new WeatherDetailAdapter(forecastDetailInfo.getWeatherDetailInfoList(),
                    getActivity().getLayoutInflater());
        } else {
            weatherDetailAdapter.setList(forecastDetailInfo.getWeatherDetailInfoList());
        }

        expandableHeightListView.setAdapter(weatherDetailAdapter);
        weatherDetailAdapter.notifyDataSetChanged();
    }

    private static class WeatherDetailAdapter extends BaseAdapter {
        private List<WeatherDetailInfo> weatherDetailInfoList;
        private LayoutInflater inflater;

        WeatherDetailAdapter(List<WeatherDetailInfo> weatherDetailInfos, LayoutInflater inflater) {
            this.weatherDetailInfoList = weatherDetailInfos;
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return weatherDetailInfoList.size();
        }

        @Override
        public WeatherDetailInfo getItem(int position) {
            return weatherDetailInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return weatherDetailInfoList.get(position).getDayOfYear();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder viewHolder;
            if (view != null) {
                viewHolder = (ViewHolder) view.getTag();
            } else {
                view = inflater.inflate(R.layout.layout_weather_detail_item, parent, false);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            }

            WeatherDetailInfo weatherDetailInfo = weatherDetailInfoList.get(position);
            viewHolder.IV_DETAIL_STATUS.setImageResource(WeatherApp.getStatusResId(weatherDetailInfo.getStatusType(),
                    weatherDetailInfo.isNight()));
            viewHolder.TV_DETAIL_DAY.setText(weatherDetailInfo.getWeekAndDate());
            viewHolder.TV_DETAIL_TIME.setText(weatherDetailInfo.getTime());
            viewHolder.TV_DETAIL_MAX.setText(weatherDetailInfo.getMaxT());
            viewHolder.TV_DETAIL_MIN.setText(weatherDetailInfo.getMinT());

            return view;
        }

        public void setList(List<WeatherDetailInfo> list) {
            this.weatherDetailInfoList = list;
        }
    }

    private static class ViewHolder {
        ImageView IV_DETAIL_STATUS;
        TextView TV_DETAIL_DAY;
        TextView TV_DETAIL_TIME;
        TextView TV_DETAIL_MAX;
        TextView TV_DETAIL_MIN;

        public ViewHolder(View view) {
             IV_DETAIL_STATUS = (ImageView) view.findViewById(R.id.IV_DETAIL_STATUS);
             TV_DETAIL_DAY = (TextView) view.findViewById(R.id.TV_DETAIL_DAY);
             TV_DETAIL_TIME = (TextView) view.findViewById(R.id.TV_DETAIL_TIME);
             TV_DETAIL_MAX = (TextView) view.findViewById(R.id.TV_DETAIL_MAX);
             TV_DETAIL_MIN = (TextView) view.findViewById(R.id.TV_DETAIL_MIN);
        }
    }
}
