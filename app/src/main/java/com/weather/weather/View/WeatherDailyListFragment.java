package com.weather.weather.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.weather.weather.MainViewModel;
import com.weather.weather.R;
import com.weather.weather.ViewModel.WeatherDetailInfo;
import com.weather.weather.WeatherApp;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class WeatherDailyListFragment extends FragmentBase<MainViewModel.ForecastDailyInfo> {
    private RecyclerView RV_WEATHER_DAILY;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_day_list, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false);
        RV_WEATHER_DAILY = (RecyclerView) view.findViewById(R.id.RV_WEATHER_DAILY);
        RV_WEATHER_DAILY.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MainViewModel.ForecastDailyInfo forecastDailyInfo) {
        Logger.d("forecastDailyInfo: %s", forecastDailyInfo);
        WeatherDailyItemAdapter weatherDailyItemAdapter = (WeatherDailyItemAdapter) RV_WEATHER_DAILY.getAdapter();
        if (weatherDailyItemAdapter == null) {
            weatherDailyItemAdapter = new WeatherDailyItemAdapter(getActivity().getLayoutInflater(),
                    forecastDailyInfo.getWeatherDetailInfoList());
        } else {
            weatherDailyItemAdapter.setList(forecastDailyInfo.getWeatherDetailInfoList());
        }

        RV_WEATHER_DAILY.setAdapter(weatherDailyItemAdapter);
    }

    private class WeatherDailyItemAdapter extends RecyclerView.Adapter<DailyItemViewHolder> implements View.OnClickListener {
        private LayoutInflater inflater;
        private List<WeatherDetailInfo> weatherDetailInfoList;
        private View selectedView;

        WeatherDailyItemAdapter(LayoutInflater inflater, List<WeatherDetailInfo> weatherDetailInfoList) {
            this.inflater = inflater;
            this.weatherDetailInfoList = weatherDetailInfoList;
        }

        @Override
        public DailyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.layout_weather_daily_item, parent, false);
            DailyItemViewHolder viewHolder = new DailyItemViewHolder(itemView);
            itemView.setTag(viewHolder);
            itemView.setOnClickListener(this);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(DailyItemViewHolder holder, int position) {
            WeatherDetailInfo weatherDetailInfo = weatherDetailInfoList.get(position);
            holder.TV_DAILY_MAX_TEMP.setText(weatherDetailInfo.getMaxT());
            holder.TV_DAILY_MIN_TEMP.setText(weatherDetailInfo.getMinT());
            holder.TV_DAILY_WEEKDAY.setText(weatherDetailInfo.getShortWeekday());
            holder.IV_WEATHER_DAILY_ITEM.setImageResource(
                    WeatherApp.getStatusResId(weatherDetailInfo.getStatusType(), false));
            holder.dayOfYear = weatherDetailInfo.getDayOfYear();
        }

        @Override
        public int getItemCount() {
            return weatherDetailInfoList.size();
        }

        public void setList(List<WeatherDetailInfo> list) {
            this.weatherDetailInfoList = list;
        }

        @Override
        public void onClick(View v) {
            DailyItemViewHolder viewHolder = (DailyItemViewHolder) v.getTag();
            ((IWeatherActionRequest) (getActivity())).getWeatherDayDetail(viewHolder.dayOfYear);

            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    private static class DailyItemViewHolder extends RecyclerView.ViewHolder {
        int dayOfYear;
        TextView TV_DAILY_WEEKDAY;
        TextView TV_DAILY_MAX_TEMP;
        TextView TV_DAILY_MIN_TEMP;
        ImageView IV_WEATHER_DAILY_ITEM;

        DailyItemViewHolder(View itemView) {
            super(itemView);
            TV_DAILY_WEEKDAY = (TextView) itemView.findViewById(R.id.TV_DAILY_WEEKDAY);
            TV_DAILY_MAX_TEMP = (TextView) itemView.findViewById(R.id.TV_DAILY_MAX_TEMP);
            TV_DAILY_MIN_TEMP = (TextView) itemView.findViewById(R.id.TV_DAILY_MIN_TEMP);
            IV_WEATHER_DAILY_ITEM = (ImageView) itemView.findViewById(R.id.IV_WEATHER_DAILY_ITEM);
        }
    }
}
