package com.weather.weather;

public final class Configs {
    public static final String FLAVOR_DEVELOP = "develop";
    public static final String FLAVOR_PRODUCT = "product";
}
