package com.weather.weather.ViewModel;


public interface IViewModelEventBusListener<EVENT> {
    // Make sure there is at least one onMessage method, in case the runtime error
    void onMessage(EVENT event);
}
