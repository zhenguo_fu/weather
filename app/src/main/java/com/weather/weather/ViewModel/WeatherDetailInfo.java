package com.weather.weather.ViewModel;

import com.orhanobut.logger.Logger;
import com.weather.weather.Model.Location;
import com.weather.weather.Model.Weather;
import com.weather.weather.Model.WeatherDetail;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

public class WeatherDetailInfo {
    private static final float KELVIN_TEMP = -273.16f;
    private static final String CELSIUS_SYMBOL = "\u00B0";
    private static final String KELVIN_SYMBOL = "\u212A";
    private static final String FAHRENHEIT_SYMBOL = "\u2109";

    private String location;
    private String maxT;
    private String minT;
    private String currentT;
    private String weekday;
    private String shortWeekday;
    private String date;
    private String status;
    private String time;
    private STATUS_TYPE statusType;
    private int dayOfYear;
    private boolean isNight;

    private WeatherDetailInfo(String location,
                              String maxT,
                              String minT,
                              String currentT,
                              String status,
                              String weekday,
                              String shortWeekday,
                              String date,
                              String time,
                              int dayOfYear,
                              int remoteStatusId,
                              boolean isNight) {
        this.location = location;
        this.maxT = maxT;
        this.minT = minT;
        this.currentT = currentT;
        this.weekday = weekday;
        this.shortWeekday = shortWeekday;
        this.date = date;
        this.time = time;
        this.status = status;
        this.dayOfYear = dayOfYear;
        this.statusType = STATUS_TYPE.getStatusByRemoteId(remoteStatusId);
        this.isNight = isNight;
    }

    public String getWeekday() {
        return weekday;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public String getLocation() {
        return location;
    }

    public STATUS_TYPE getStatusType() {
        return statusType;
    }

    public String getShortWeekday() {
        return shortWeekday;
    }

    public String getTime() {
        return time;
    }

    public String getWeekAndDate() {
        return weekday + " " + date;
    }

    public String getMaxT() {
        return maxT;
    }

    public String getMinT() {
        return minT;
    }

    public String getCurrentT() {
        return currentT;
    }

    public boolean isNight() {
        return isNight;
    }

    public void setMaxT(String maxT) {
        this.maxT = maxT;
    }

    public void setMinT(String minT) {
        this.minT = minT;
    }

    @Override
    public String toString() {
        return String.format("location: %s, max: %s, min: %s, current: %s, weekDay: %s, shortWeekday: %s, date: %s, status: %s, dayOfYear: %s, statusType: %s",
                location,
                maxT,
                minT,
                currentT,
                weekday,
                shortWeekday,
                date,
                status,
                dayOfYear,
                statusType);
    }

    public enum STATUS_TYPE {
        THUNDERSTORM,
        DRIZZLE,
        SUNNY,
        RAINY,
        CLOUDY,
        FOGGY,
        SNOW,
        UNKNOWN;

        public static STATUS_TYPE getStatusByRemoteId(int remoteId) {
            STATUS_TYPE result;
            int localId = remoteId / 100;
            switch (localId) {
                case 2:
                    result = THUNDERSTORM;
                    break;
                case 3:
                    result = DRIZZLE;
                    break;
                case 5:
                    result = RAINY;
                    break;
                case 6:
                    result = SNOW;
                    break;
                case 7:
                    result = FOGGY;
                    break;
                case 8:
                    if (remoteId > 800) {
                        result = CLOUDY;
                    } else {
                        result = SUNNY;
                    }
                    break;
                default:
                    result = UNKNOWN;
                    Logger.w("Unknown or special status: %s", remoteId);
                    break;
            }

            return result;
        }
    }

    public enum TEMPERATURE_TYPE {
        KELVIN {
            @Override
            public float getDegree(float temp) {
                return temp;
            }

            @Override
            public String getSymbol() {
                return KELVIN_SYMBOL;
            }
        },
        CELSIUS {
            @Override
            public float getDegree(float temp) {
                return temp + KELVIN_TEMP;
            }

            @Override
            public String getSymbol() {
                return CELSIUS_SYMBOL;
            }
        },
        FAHRENHEIT {
            @Override
            public float getDegree(float temp) {
                return temp * 9f / 5f - 459.67f;
            }

            @Override
            public String getSymbol() {
                return FAHRENHEIT_SYMBOL;
            }
        },
        ;


        public abstract float getDegree(float temp);
        public abstract String getSymbol();

        public String getDegreeString(float temp) {
            return String.valueOf((int) getDegree(temp)) + getSymbol();
        }
    }

    public static WeatherDetailInfo convertWeatherDetailInfo(WeatherDetail weatherDetail,
                                                             TEMPERATURE_TYPE temperatureType) {
        Location location = weatherDetail.getLocation();
        Weather weather = weatherDetail.getWeather();
        DateTime dateTime = weatherDetail.getDateTime();

        return new WeatherDetailInfo(
                location.getLocalityName(),
                temperatureType.getDegreeString(weather.getMaxT()),
                temperatureType.getDegreeString(weather.getMinT()),
                temperatureType.getDegreeString(weather.getCurrentT()),
                weather.getStatus(),
                dateTime.toLocalDate().equals(new LocalDate()) ? "Today" : dateTime.dayOfWeek().getAsText(),
                dateTime.toLocalDate().equals(new LocalDate()) ? "Today" : dateTime.dayOfWeek().getAsShortText(),
                dateTime.toLocalDate().toString("MMM dd"),
                dateTime.toLocalTime().toString("HH:mm"),
                dateTime.getDayOfYear(),
                weatherDetail.getWeather().getRemoteStatusId(),
                isNight(dateTime.toLocalTime().getHourOfDay()));
    }

    //TODO: add check with the sunset and sunrise time of location
    private static boolean isNight(int hour) {
        return hour > 18 || hour < 6;
    }
}
