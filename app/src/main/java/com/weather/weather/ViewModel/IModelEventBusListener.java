package com.weather.weather.ViewModel;


public interface IModelEventBusListener<T> {
    // Make sure there is at least one onMessage method, in case the runtime error
    void onMessage(T t);
}
