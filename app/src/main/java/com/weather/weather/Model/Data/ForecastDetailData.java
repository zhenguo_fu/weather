package com.weather.weather.Model.Data;

import java.util.List;

public class ForecastDetailData {
    public long dt;
    public int id;
    public ForecastMainData main;
    public List<ForecastWeatherData> weather;
}
