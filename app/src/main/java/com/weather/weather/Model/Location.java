package com.weather.weather.Model;

public class Location implements IModel {
    private Double latitude;
    private Double longitude;
    private String localityName;

    public Location(android.location.Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public Location(Double latitude, Double longitude, String localityName) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.localityName = localityName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getLocalityName() {
        return localityName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!Location.class.isInstance(obj)) {
            return false;
        }

        Location theLocation = (Location) obj;
        return theLocation.getLocalityName().equals(localityName);
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }
}
