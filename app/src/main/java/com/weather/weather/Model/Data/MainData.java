package com.weather.weather.Model.Data;

public class MainData {
    public float temp;
    public float temp_min;
    public float temp_max;
    public int humidity;
    public float pressure;
}
