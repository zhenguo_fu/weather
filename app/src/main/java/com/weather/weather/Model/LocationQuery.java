package com.weather.weather.Model;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationQuery
        extends BaseQuery<LocationList, Location>
        implements  com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final long MINIMUM_LOCATION_UPDATE_PERIOD = 5000;
    private static final float SMALLEST_DISPLACEMENT_METERS = 10.0f;

    private Boolean oneshot;
    private long requestInterval;
    private GoogleApiClient googleApiClient;
    private android.location.Location defaultLocation;
    private Boolean resultDeliveredAtLeastOnce;
    private Boolean isConnectedToApi = false;
    private LocationList locationList;

    public LocationQuery (Context context, EventBus eventBus, LocationList locationList, Long requestInterval) {
        super(context, eventBus);
        this.locationList = locationList;
        this.defaultLocation = null;
        this.resultDeliveredAtLeastOnce = false;

        this.oneshot = false;
        if (requestInterval == null || requestInterval == 0) {
            oneshot = true;
            this.requestInterval = 0;
        } else {
            this.requestInterval = requestInterval;
        }

        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

    private void stopUpdates() {
        if (isConnectedToApi) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }

        isConnectedToApi = false;
    }

    private void noLocationAvailable() {
        Logger.e("Location note available!");
        locationList.getList().clear();
        postResultList(locationList);
    }

    private void deliverResult(android.location.Location theLocation) {
        final Location location = new Location(theLocation);
        new AsyncTask<android.location.Location, Void, String>() {
            @Override
            protected String doInBackground(android.location.Location... params) {
                return getLocalityByLocation(context, params[0].getLatitude(), params[0].getLongitude());
            }

            @Override
            protected void onPostExecute(String localityName) {
                Logger.d("localityName: %s", localityName);
                location.setLocalityName(localityName);
                locationList.getList().clear();
                locationList.getList().add(location);
                postResultList(locationList);
                resultDeliveredAtLeastOnce = true;
            }
        }.execute(theLocation);
    }

    @Override
    public void onDestroy() {
        Logger.d("");
        super.onDestroy();
        stopUpdates();
    }

    /**
     * LocationRequest callbacks.
     */
    @Override
    public void onLocationChanged(android.location.Location location) {
        Logger.d("location: %s", location);
        if (location != null) {
            deliverResult(location);
        } else {
            noLocationAvailable();
        }

        if (oneshot) {
            stopUpdates();
        }
    }

    /**
     * Google Api connection callbacks.
     */
    @Override
    public void onConnected(Bundle bundle) {
        Logger.d("bundle: %s", bundle);
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(requestInterval);

        locationRequest.setFastestInterval(MINIMUM_LOCATION_UPDATE_PERIOD);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_METERS);

        defaultLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this, Looper.getMainLooper());

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!resultDeliveredAtLeastOnce) {
                    onLocationChanged(defaultLocation);
                }
            }
        }, requestInterval + 1000L);

        isConnectedToApi = true;
    }

    @Override
    public void onConnectionSuspended(int i) {
        noLocationAvailable();
        isConnectedToApi = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        noLocationAvailable();
        isConnectedToApi = false;
    }

    private String getLocalityByLocation(Context context, Double latitude, Double longitude) {
        Logger.d("activity: %s, lat: %s, log: %s",
                context,
                latitude,
                longitude);
        String locality = "";
        if (Geocoder.isPresent()) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses.size() > 0) {
                    locality = addresses.get(0).getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return locality;
    }

}
