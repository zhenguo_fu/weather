package com.weather.weather.Model;

import java.util.ArrayList;
import java.util.List;

public abstract class ModelList<M extends IModel> {
    protected List<M> list;

    public ModelList() {
        this.list = new ArrayList<>();
    }

    public List<M> getList() {
        return list;
    }
}
