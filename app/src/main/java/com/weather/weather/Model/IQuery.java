package com.weather.weather.Model;

public interface IQuery<ML extends ModelList<M>, M extends IModel> {
    void postResultList(ML modelList);
}
