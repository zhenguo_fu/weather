package com.weather.weather.Model.Network;

import com.orhanobut.logger.Logger;
import com.weather.weather.Model.ModelList;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Response;

public abstract class CallbackBase<T, ML extends ModelList> implements retrofit2.Callback<T>{
    private static final int HTTP_SUCCESS = 200;
    private static final int HTTP_REDIRECT = 300;

    protected ML resultList;

    public CallbackBase(ML resultList) {
        this.resultList = resultList;
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Logger.e("Request: %s", call.request());
        Logger.e("Failed message: %s", t.getMessage());
        Logger.e("Error stack: %s", Arrays.asList(t.getStackTrace()));
    }

    protected boolean validateResponse(Response response) {
        boolean result = false;
        int responseCode = response.code();
        if (response.isSuccessful() && responseCode >= HTTP_SUCCESS && responseCode < HTTP_REDIRECT) {
            result = true;
        } else {
            Logger.e("Http request failed");
        }

        return result;
    }

}
