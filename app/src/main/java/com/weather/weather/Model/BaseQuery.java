package com.weather.weather.Model;

import android.content.Context;

import com.orhanobut.logger.Logger;
import com.weather.weather.Dagger.DaggerNetComponent;
import com.weather.weather.Dagger.NetComponent;
import com.weather.weather.Dagger.NetModule;

import org.greenrobot.eventbus.EventBus;


public abstract class BaseQuery<ML extends ModelList<M>, M extends IModel> implements IQuery<ML, M> {
    private EventBus eventBus;
    protected Context context;
    protected NetComponent netComponent;

    public BaseQuery(Context context, EventBus eventBus) {
        this.context = context;
        this.eventBus = eventBus;

        netComponent = DaggerNetComponent.builder()
                .netModule(new NetModule())
                .build();
    }

    @Override
    public void postResultList(ML resultList) {
        Logger.d("resultList: %s", resultList);
        eventBus.post(resultList);
    }

    public void onDestroy() {}
}
