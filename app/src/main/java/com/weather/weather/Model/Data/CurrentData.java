package com.weather.weather.Model.Data;

import java.util.List;

public class CurrentData {
    public List<WeatherData> weather;
    public MainData main;
    public String name;
    public CoordData coord;
    public long dt;
}
