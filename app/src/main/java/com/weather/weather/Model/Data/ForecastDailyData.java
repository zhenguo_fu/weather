package com.weather.weather.Model.Data;

import java.util.List;

public class ForecastDailyData {
    public CityData city;
    public List<ForecastDailyDetailData> list;
}
