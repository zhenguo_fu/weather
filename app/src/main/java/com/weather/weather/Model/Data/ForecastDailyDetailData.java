package com.weather.weather.Model.Data;

import java.util.List;

public class ForecastDailyDetailData {
    public long dt;
    public ForecastDailyTempData temp;
    public List<ForecastWeatherData> weather;
}
