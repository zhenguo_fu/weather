package com.weather.weather.Model.Network;

import com.weather.weather.Model.Data.CurrentData;
import com.weather.weather.Model.Data.ForecastDailyData;
import com.weather.weather.Model.Data.ForecastData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPI {
    @GET("data/2.5/weather?")
    Call<CurrentData> loadCurrentWeather(@Query("lat") String lat,
                                         @Query("lon") String lon,
                                         @Query("appid") String appid);

    @GET("data/2.5/forecast?")
    Call<ForecastData> loadForecastDetail(@Query("lat") String lat,

                                          @Query("lon") String lon,
                                          @Query("appid") String appid);

    @GET("data/2.5/forecast/daily?")
    Call<ForecastDailyData> loadForecastDaily(@Query("lat") String lat,
                                               @Query("lon") String lon,
                                               @Query("appid") String appid,
                                               @Query("cnt") String count);

}
