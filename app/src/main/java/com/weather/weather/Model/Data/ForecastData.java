package com.weather.weather.Model.Data;

import java.util.List;

public class ForecastData {
    public CityData city;
    public List<ForecastDetailData> list;
}
