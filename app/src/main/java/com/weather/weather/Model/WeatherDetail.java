package com.weather.weather.Model;


import org.joda.time.DateTime;

public class WeatherDetail implements IModel {
    private Location location;
    private DateTime dateTime;
    private Weather weather;

    public WeatherDetail(Location location, DateTime dateTime, Weather weather) {
        this.location = location;
        this.dateTime = dateTime;
        this.weather = weather;
    }

    public Location getLocation() {
        return location;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public Weather getWeather() {
        return weather;
    }

    @Override
    public String toString() {
        return String.format("location: %s, dateTime:%s, weather: %s", location, dateTime, weather);
    }
}
