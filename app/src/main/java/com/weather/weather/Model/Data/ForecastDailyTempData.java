package com.weather.weather.Model.Data;

public class ForecastDailyTempData {
    public float day;
    public float min;
    public float max;
    public float night;
    public float eve;
    public float morn;
}
