package com.weather.weather.Model;

import android.content.Context;

import com.weather.weather.Model.Data.CurrentData;
import com.weather.weather.Model.Data.ForecastDailyData;
import com.weather.weather.Model.Data.ForecastDailyDetailData;
import com.weather.weather.Model.Data.ForecastData;
import com.weather.weather.Model.Data.ForecastDetailData;
import com.weather.weather.Model.Network.CallbackBase;
import com.weather.weather.Model.Network.WeatherAPI;
import com.weather.weather.R;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WeatherQuery extends BaseQuery<WeatherDetailList, WeatherDetail> {
    private static final String DAILY_COUNT = "16";
    private static final long SECOND = 1000L;

    private String api_key;

    @Inject
    Retrofit retrofit;

    public WeatherQuery(Context context, EventBus eventBus) {
        super(context, eventBus);
        api_key = context.getString(R.string.api_key);
        netComponent.inject(this);
    }

    public void queryCurrent(Location location, WeatherDetailList weatherDetailList) {
        WeatherAPI weatherAPI = retrofit.create(WeatherAPI.class);
        Call<CurrentData> call = weatherAPI.loadCurrentWeather(location.getLatitude().toString(),
                location.getLongitude().toString(),
                api_key);
        call.enqueue(new CurrentCallback(weatherDetailList));
    }

    public void queryForecastDetail(Location location, WeatherDetailList weatherDetailList) {
        WeatherAPI weatherAPI = retrofit.create(WeatherAPI.class);
        Call<ForecastData> call = weatherAPI.loadForecastDetail(location.getLatitude().toString(),
                location.getLongitude().toString(),
                api_key);
        call.enqueue(new ForecastDetailCallback(weatherDetailList));
    }

    public void queryForecastDaily(Location location, WeatherDetailList weatherDetailList) {
        WeatherAPI weatherAPI = retrofit.create(WeatherAPI.class);
        Call<ForecastDailyData> call = weatherAPI.loadForecastDaily(location.getLatitude().toString(),
                location.getLongitude().toString(),
                api_key,
                DAILY_COUNT);
        call.enqueue(new ForecastDailyCallback(weatherDetailList));
    }



    private class ForecastDailyCallback extends CallbackBase<ForecastDailyData, WeatherDetailList> {
        public ForecastDailyCallback(WeatherDetailList resultList) {
            super(resultList);
        }

        @Override
        public void onResponse(Call<ForecastDailyData> call, Response<ForecastDailyData> response) {
            if (!validateResponse(response)) {
                return;
            }

            resultList.getList().clear();
            ForecastDailyData forecastDailyData = response.body();
            Location location = new Location(forecastDailyData.city.coord.lat,
                    forecastDailyData.city.coord.lon,
                    forecastDailyData.city.name);
            for (ForecastDailyDetailData detailData : forecastDailyData.list) {
                DateTime dateTime = new DateTime(detailData.dt * SECOND);
                Weather weather = new Weather(detailData.temp.max,
                        detailData.temp.min,
                        detailData.temp.day,
                        detailData.weather.get(0).main,
                        detailData.weather.get(0).id);
                WeatherDetail weatherDetail = new WeatherDetail(location, dateTime, weather);
                resultList.getList().add(weatherDetail);
            }

            postResultList(resultList);
        }
    }

    private class ForecastDetailCallback extends CallbackBase<ForecastData, WeatherDetailList> {
        public ForecastDetailCallback(WeatherDetailList resultList) {
            super(resultList);
        }

        @Override
        public void onResponse(Call<ForecastData> call, Response<ForecastData> response) {
            if (!validateResponse(response)) {
                return;
            }

            //TODO: code smells not good, should be generalized with upper one
            resultList.getList().clear();
            ForecastData forecastData = response.body();
            Location location = new Location(forecastData.city.coord.lat,
                    forecastData.city.coord.lon,
                    forecastData.city.name);
            for (ForecastDetailData detail: forecastData.list) {
                DateTime dateTime = new DateTime(detail.dt * SECOND);
                Weather weather = new Weather(detail.main.temp_max,
                        detail.main.temp_min,
                        detail.main.temp,
                        detail.weather.get(0).main,
                        detail.weather.get(0).id);
                WeatherDetail weatherDetail = new WeatherDetail(location, dateTime, weather);
                resultList.getList().add(weatherDetail);
            }

            postResultList(resultList);
        }
    }

    private class CurrentCallback extends CallbackBase<CurrentData, WeatherDetailList> {
        public CurrentCallback(WeatherDetailList modelList) {
            super(modelList);
        }

        @Override
        public void onResponse(Call<CurrentData> call, Response<CurrentData> response) {
            if (!validateResponse(response)) {
                return;
            }

            CurrentData currentData = response.body();
            Location location = new Location(currentData.coord.lat,
                    currentData.coord.lon,
                    currentData.name);
            DateTime dateTime = new DateTime(currentData.dt * SECOND);
            Weather weather = new Weather(currentData.main.temp_max,
                    currentData.main.temp_min,
                    currentData.main.temp,
                    currentData.weather.get(0).main,
                    currentData.weather.get(0).id);
            WeatherDetail weatherDetail = new WeatherDetail(location, dateTime, weather);
            resultList.getList().clear();
            resultList.getList().add(weatherDetail);
            postResultList(resultList);
        }
    }

}
