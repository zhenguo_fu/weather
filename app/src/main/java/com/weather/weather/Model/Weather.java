package com.weather.weather.Model;

public class Weather {
    private float maxT;
    private float minT;
    private float currentT;
    private String status;
    private int remoteStatusId;

    public Weather(float maxT, float minT, float currentT, String status, int remoteStatusId) {
        this.maxT = maxT;
        this.minT = minT;
        this.currentT = currentT;
        this.status = status;
        this.remoteStatusId = remoteStatusId;
    }

    public float getMaxT() {
        return maxT;
    }

    public float getMinT() {
        return minT;
    }

    public float getCurrentT() {
        return currentT;
    }

    public String getStatus() {
        return status;
    }

    public int getRemoteStatusId() {
        return remoteStatusId;
    }

    @Override
    public String toString() {
        return String.format("max: %s, min: %s, current: %s, status: %s, remoteId: %s",
                maxT,
                minT,
                currentT,
                status,
                remoteStatusId);
    }
}
