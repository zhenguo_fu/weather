package com.weather.weather.Dagger;

import com.google.gson.Gson;
import com.weather.weather.Model.WeatherQuery;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = NetModule.class)
public interface NetComponent {
    void inject(WeatherQuery weatherQuery);

    NetModule provideNetModule();
    @Named("baseUrl") String provideBaseUrl();
    Gson provideGson();
    OkHttpClient provideClient();
    Retrofit provideRetrofit();
}


