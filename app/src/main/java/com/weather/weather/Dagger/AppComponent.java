package com.weather.weather.Dagger;

import android.app.Activity;
import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(Activity activity);
    Application provideWeatherApp();
}
