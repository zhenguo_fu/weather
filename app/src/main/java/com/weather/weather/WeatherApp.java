package com.weather.weather;

import android.app.Application;

import com.weather.weather.Dagger.AppModule;
import com.weather.weather.Dagger.DaggerAppComponent;
import com.weather.weather.ViewModel.WeatherDetailInfo;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.HashMap;
import java.util.Map;


public class WeatherApp extends Application {

    private static final Map<WeatherDetailInfo.STATUS_TYPE, Integer> DAY_STATUS_ICON_MAP = new HashMap<>();
    static {
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.THUNDERSTORM, R.drawable.icon_thunder);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.DRIZZLE, R.drawable.icon_drizzle);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.SUNNY, R.drawable.icon_sunny);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.RAINY, R.drawable.icon_rainy);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.CLOUDY, R.drawable.icon_cloudy);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.FOGGY, R.drawable.icon_foggy);
        WeatherApp.DAY_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.SNOW, R.drawable.icon_snow);
    }

    private static final Map<WeatherDetailInfo.STATUS_TYPE, Integer> NIGHT_STATUS_ICON_MAP = new HashMap<>();
    static {
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.THUNDERSTORM, R.drawable.icon_night_thunder);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.DRIZZLE, R.drawable.icon_night_drizzle);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.SUNNY, R.drawable.icon_night_sunny);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.RAINY, R.drawable.icon_night_rainy);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.CLOUDY, R.drawable.icon_night_cloudy);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.FOGGY, R.drawable.icon_night_foggy);
        WeatherApp.NIGHT_STATUS_ICON_MAP.put(WeatherDetailInfo.STATUS_TYPE.SNOW, R.drawable.icon_night_snow);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);

        DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static int getStatusResId(WeatherDetailInfo.STATUS_TYPE statusType, boolean isNight) {
        return isNight ? NIGHT_STATUS_ICON_MAP.get(statusType) : DAY_STATUS_ICON_MAP.get(statusType);
    }
}
